import hmac
import hashlib
import json
import requests
import time


def get_binance_price(pair_text):
    url= 'https://api.binance.com/api/v3/ticker/price'
    if (pair_text):
        x = requests.get(url,params={'symbol': pair_text})
    else:
        x = requests.get(url)
    return x.json()



pair_text = 'xrp'.upper()+'USDT'

'''
#last trades up to number : limit trades
current_trades_to_fetch = 1
url= 'https://api.binance.com/api/v3/trades'
print(pair_text)
x = requests.get(url,params={'symbol': pair_text,'limit':current_trades_to_fetch})
print(x.text)


url= 'https://api.binance.com/api/v3/avgPrice'
print(pair_text)
#average price last 5 minuntes
x = requests.get(url,params={'symbol': pair_text})
print(x.text)
'''
#Latest price for a symbol or all symbols if no input
# /api/v3/ticker/price
