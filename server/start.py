import sqlite3
conn = sqlite3.connect('cryptoCoins.db')

c = conn.cursor()

# Create table
c.execute("""CREATE TABLE coins(
        coinName text PRIMARY KEY,
        quantity real NOT NULL)""")

# Insert a row of data
c.execute("INSERT INTO coins VALUES ('XRP',12.12)")
c.execute("INSERT INTO coins VALUES ('USDT',299.5)")
c.execute("INSERT INTO coins VALUES ('ETH',46546)")
c.execute("INSERT INTO coins VALUES ('CRO',1777)")

# Save (commit) the changes
#conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
#conn.close()
##create the transactions table
#c = conn.cursor()

c.execute("""CREATE TABLE transactions(
        tx_id INTEGER PRIMARY KEY,
        token_from string NOT NULL,
        token_to string NOT NULL,
        quantity_from real NOT NULL,
        price_to_from real NOT NULL
        )""")
c.execute("INSERT INTO transactions(token_from, token_to, quantity_from,  price_to_from) VALUES ('XRP','USDT',12 ,45)")
c.execute("INSERT INTO transactions(token_from, token_to, quantity_from,  price_to_from) VALUES ('XRP','CRO',120 ,445)")
c.execute("INSERT INTO transactions(token_from, token_to, quantity_from,  price_to_from) VALUES ('ETH','CRO',41 ,5)")
c.execute("INSERT INTO transactions(token_from, token_to, quantity_from,  price_to_from) VALUES ('ETH','XRP',641 ,556)")
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
