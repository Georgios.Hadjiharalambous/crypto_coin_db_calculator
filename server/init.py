from flask import Flask, render_template, url_for,jsonify
from flask import make_response, request


import json


from bnb import get_binance_price
import sqlite3
from functions import *

app = Flask(__name__)
app.debug = True

@app.route('/')
#pellara xx
def initial_page():
   return render_template("index.html")

''''
COINS END POINT
'''
@app.route("/api/coins/", methods=['GET'])
def get_all_coins():
   conn = sqlite3.connect('cryptoCoins.db')
   c = conn.cursor()
   print('results for all coins')
   c.execute('SELECT * FROM coins')
   temp = c.fetchall()
   result = {}
   for row in temp:
      result[row[0]]={'quantity':row[1]}
   return jsonify({"coins":result})


@app.route("/api/coins/<string:coin_name>", methods=['GET'])
def get_coin(coin_name):
   coin_name = coin_name.upper(  )
   conn = sqlite3.connect('cryptoCoins.db')
   c = conn.cursor()
   print('coin name received :'+coin_name)
   c.execute('SELECT * FROM coins WHERE coinName=?',[coin_name])
   row = c.fetchone()#here I say fetchONE not fetchALL!!!!!!!!
   result={'name':row[0],'quantity':row[1]}

   return jsonify(result)
##TODOOOO
@app.route('/coins/', methods=['GET'])
def render_coins():
   return get_all_coins()

@app.route('/coins/<string:coin_name>', methods=['GET'])
def render_coin(coin_name):
   return render_template("coin.html") # or you can called render_template("coin.html",data=get_coin(coin_name)) and make the necessary changes to the coin.html

@app.route('/api/coins/insert', methods=['POST'])
def call_insert_coin():
   if request.method == 'POST':
      data = request.get_json()
      return insert_coin(data)


''''
TRANSACTIONS END POINT
'''
@app.route("/api/transactions/", methods=['GET'])
def get_all_transaction():
   conn = sqlite3.connect('cryptoCoins.db')
   c = conn.cursor()
   print('ALL transaction')
   c.execute('SELECT * FROM transactions ')
   temp = c.fetchall()
   result = {}
   for row in temp:
      result[row[0]] = {'token_from':row[1], 'token_to':row[2], 'quantity_from':row[3],  'price_to_from':row[4]}
   #return json.dumps(result, indent=2)
   return jsonify(result)


@app.route("/api/transactions/<string:coin_name>", methods=['GET','POST'])
def transaction(coin_name):
   coin_name = coin_name.upper()

   if request.method == 'GET':
      get_transaction(coin_name)
   else :
      return "lathos"



@app.route("/api/transactions/insert", methods=['POST'])
def insert_tx():
   if request.method == 'POST':
      data = request.get_json()
      return insert_transaction(data)




@app.route("/api/bnb/price/<string:coin_name>", methods=['GET'])
def get_price_from_binance(coin_name):
   return get_binance_price(coin_name)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found je bromas jiolas re blakaaaa'}), 404)
